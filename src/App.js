import React from 'react';

//React_Router_DOM
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { createBrowserHistory as history} from 'history';

//Custom CSS for all components
import './components/forall.css';

//Axios Config Import 
import './config/axiosconfig';

//Components Import 
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import Home from './components/Home/Home';
import Terms from './components/Terms/Terms';
import Privacy from './components/Privacy/Privacy';
import Listing from './components/Listing/Listing';
import Agents from './components/Agents/Agents';
import Details from './components/Details/Details';
import Profile from './components/Profile/Profile';
import ProfileListing from './components/Profile/ProfileListing/ProfileListing';
import ProfileFav from './components/Profile/ProfileFav/ProfileFav';
import ProfileContact from './components/Profile/ProfileContact/ProfileContact';
import AccountSettings from './components/AccountSettings/AccountSettings';
import EditProfile from './components/AccountSettings/EditProfile/EditProfile';
import ChangePassword from './components/AccountSettings/ChangePassword/ChangePassword';
import ChangePhotos from './components/AccountSettings/ChangePhotos/ChangePhotos';
import NewListingPage from './components/NewListingPage/NewListingPage';

// FontAwesome Imports
import {library} from '@fortawesome/fontawesome-svg-core';
import {faUnlockAlt, faBed, faBath, faVectorSquare,faChevronDown,
        faEye, faShare, faHeart, faParking,faCalendar,faMapPin,
        faSink, faWind, faChair, faDesktop, faStamp, faCouch, faUtensilSpoon,
        faDollyFlatbed, faSwimmingPool, faWarehouse, faLandmark, faPhone,
        faChevronLeft, faChevronRight, faChevronUp, faImages, faStar, faMapMarked, faClone,
        faFilter, faClock, faSearch, faLock, faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons'
import {faFacebookSquare, faGooglePlusSquare, faRedditAlien,
   faWhatsapp, faFacebookMessenger, faTwitter, faFacebook, faInstagram} from '@fortawesome/free-brands-svg-icons';
import {} from '@fortawesome/free-solid-svg-icons'







library.add(faUnlockAlt, faFacebookSquare, faShare, faHeart, 
  faGooglePlusSquare, faBed, faBath, faVectorSquare, faEye, faParking,
  faSink, faWind, faChair, faDesktop, faStamp, faCouch, faUtensilSpoon,
  faDollyFlatbed, faSwimmingPool, faWarehouse, faLandmark, faCalendar,
  faPhone,faChevronDown, faChevronLeft, faChevronRight, faImages, faStar,
   faMapMarked, faMapPin, faRedditAlien, faWhatsapp, faFacebookMessenger,
   faTwitter, faFacebook, faClone, faFilter, faInstagram, faClock, 
   faSearch, faChevronUp, faLock, faMapMarkerAlt);



function App() {
  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route path="/terms" component={Terms} />
          <Route path="/privacy" component={Privacy} />
          <Route path="/listing" component={NewListingPage}>
            <Route path="/listing/buy" component={(props)=> (
              <NewListingPage {...props} cat="buy" />
            )} />
            <Route path="/listing/rent" component={(props)=> (
              <NewListingPage {...props} cat="rent" />
            )} />
          </Route>
          <Route path="/agents" component={Agents} />
          <Route path="/posts/:id" render={props=>(
            <Details {...props} />
          )} />
          <Route path="/profile/listing" exact component={ProfileListing} />
          <Route path="/profile/favorite" exact component={ProfileFav} />
          <Route path="/profile/contact" exact component={ProfileContact} />
          <Route path="/account-settings/edit-profile" exact component={EditProfile} />
          <Route path="/account-settings/change-photos" exact component={ChangePhotos} />
          <Route path="/account-settings/change-password" exact component={ChangePassword} />
        </Switch>
      </Router>
      
    </div>
  );
}

export default App;
